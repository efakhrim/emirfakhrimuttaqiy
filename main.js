function validateForm() 
{
    if (document.forms["formPendaftaran"]["nama"].value == "") {
        alert("Nama Tidak Boleh Kosong");
        document.forms["formPendaftaran"]["nama"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["nik"].value == "") {
        alert("NIK Tidak Boleh Kosong");
        document.forms["formPendaftaran"]["nik"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["jk"].value == "") {
        alert("Pilih Jenis Kelamin.");
        document.forms["formPendaftaran"]["jk"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["tanggal"].value == "") {
        alert("Isi Tanggal Lahirmu.");
        document.forms["formPendaftaran"]["tanggal"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["alamat"].value == "") {
        alert("Cantumkan Alamatmu.");
        document.forms["formPendaftaran"]["alamat"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["agama"].selectedIndex < 1) {
        alert("Pilih Agama.");
        document.forms["formPendaftaran"]["agama"].focus();
        return false;
    }
    if (document.forms["formPendaftaran"]["nomor"].value == "") {
        alert("Cantumkan Nomor Telepon.");
        document.forms["formPendaftaran"]["nomor"].focus();
        return false;
    }
}
